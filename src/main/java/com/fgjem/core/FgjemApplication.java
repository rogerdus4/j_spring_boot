package com.fgjem.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FgjemApplication {

	public static void main(String[] args) {
		SpringApplication.run(FgjemApplication.class, args);
	}

}
