package com.fgjem.core.model;

import java.util.Date;

public class Post {
	private int id;
	private String descripcion;
	private String urlImg;
	private Date fecha;
	private String tituloString;
	
	
	public Post(int id, String descripcion, String urlImg, java.util.Date fecha, String tituloString) {
		
		this.id = id;
		this.descripcion = descripcion;
		this.urlImg = urlImg;
		this.fecha = fecha;
		this.tituloString = tituloString;
	}
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getUrlImg() {
		return urlImg;
	}
	public void setUrlImg(String urlImg) {
		this.urlImg = urlImg;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public String getTituloString() {
		return tituloString;
	}
	public void setTituloString(String tituloString) {
		this.tituloString = tituloString;
	}

	public Post() {
		
	}
	
	
}
