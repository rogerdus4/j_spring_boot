package com.fgjem.core.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.Date;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.fgjem.core.configuration.Paginas;
import com.fgjem.core.model.Post;

@Controller
@RequestMapping("/home")
public class ControllerBasic {
	
	
	public List<Post> getPosts(){
		
		ArrayList<Post> post = new ArrayList<>();
		
		post.add(new Post(1,"Desarrollo web es un término que define la creación de sitios web para Internet o una intranet","http://localhost:8080/img/ingress_.png", new Date(),"La medallita Via Noir"));
		post.add(new Post(2,"Desarrollo web es un término que define la creación de sitios web para Internet o una intranet","http://localhost:8080/img/ingress_.png", new Date(),"El ingress"));
		post.add(new Post(3,"Desarrollo web es un término que define la creación de sitios web para Internet o una intranet","http://localhost:8080/img/ingress_.png", new Date(),"The game"));
		post.add(new Post(4,"Desarrollo web es un término que define la creación de sitios web para Internet o una intranet","http://localhost:8080/img/ingress_.png", new Date(),"Niantic"));
		return post;
		
	}
	///Model View Controller
	@GetMapping(path = {"/posts","/"})
	public String saludar(Model model) {
		model.addAttribute("posts",this.getPosts());
		
		return "index";
	}

		/// Model and View 
	@GetMapping(path = "/public")
	public ModelAndView post() {
		
		ModelAndView modelAndView = new ModelAndView(Paginas.HOME);
		modelAndView.addObject("post", this.getPosts());
		
		return modelAndView;
	}
	
	@GetMapping(path = {})
	public ModelAndView getPostIndividual(
			@RequestParam(defaultValue = "1", name = "id",required = false ) int id
			) {
		ModelAndView modelAndView = new ModelAndView(Paginas.POST);
		List<Post> postsFiltradoList = this.getPosts().stream()
									.filter((p) ->{
										return p.getId() == id;
									}).collect(Collectors.toList());
		
		modelAndView.addObject("post",postsFiltradoList.get(0));
		
		return modelAndView;
	}	
	

}
